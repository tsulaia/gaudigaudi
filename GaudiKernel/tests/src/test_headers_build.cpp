/** @file
 * Force the compilation of header files that otherwise would not be read.
 */

// VectorMap.cpp
#include "GaudiKernel/HashMap.h"
#include "GaudiKernel/Map.h"
#include "GaudiKernel/VectorMap.h"
#include <stdexcept>

// HistoProperty.cpp
#include "GaudiKernel/HistoProperty.h"

/// Empty executable body, just to please the compiler/linker.
int main() { return 0; }
